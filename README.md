# NGINX / OpenResty HTAuth via MySQL DB

- [NGINX / OpenResty HTAuth via MySQL DB](#nginx--openresty-htauth-via-mysql-db)
  - [Purpose](#purpose)
  - [Requirements](#requirements)
  - [Setup](#setup)
  - [Configuration](#configuration)
  - [Queries](#queries)
  - [Database](#database)
  - [Passwords](#passwords)
    - [Generating with OpenSSL](#generating-with-openssl)
    - [Generating with Dovecot admin](#generating-with-dovecot-admin)

## Purpose

This repo contains the necessary LUA script and nginx configuration entries to configure NGINX / OpenResty to query a MySQL db for htauth information, enabling htauth authentication without maintaining an htauth file.

## Requirements

 - An existing MySQL Database
 - NGINX with LUA capabilities, or OpenResty
 - MySQL LUA library made available to NGINX (Already provided with Openresty)
 - An NGINX Server configuration
 - `openssl` or `doveadm` for generating passwords

## Setup

1. Create Directory /etc/nginx/lua-scripts.d
2. Place the `mysql-htpasswd.lua` into /etc/nginx/lua-scripts.d/
3. Build an SQL Query to return the `password` field of your database
4. Add the contents of `nginx.config` into your nginx host config
5. Set the required entries, as noted in the Configuration section
6. Restart NGINX/OpenResty
7. Open your browser to your domain, and you should be prompted for auth

## Configuration

To simplify setup, the necessary information required for the LUA script, is kept in the nginx host configuration instead, enabling the LUA script to be reused across many nginx hosts.

Required information in the `nginx.config` needs to be entered for each of the `%CONF%` entries. The required information is noted below.

 - `MY_AUTH_REALM`: htauth Authentication Realm
 - `MYSQL_DB_PASSWORD`: MySQL DB Password
 - `MYSQL_DB_USER`: MySQL DB Username
 - `MYSQL_DB_HOST`: MySQL DB Host
 - `MYSQL_DB_NAME`: MySQL Database Name
 - `FLAG_ENABLE_MYSQL_SSL`: (bool) Enable MySQL SSL/TLS connection
 - `MY_MYSQL_QUERY`: SQL Query to obtain the necessary information (See Queries Section)
 - `MY_DOMAIN`: Your Domain

The NGINX configuration flags for `lua_ssl_*` in `nginx.config` can be commented out or omitted if `FLAG_ENABLE_MYSQL_SSL` will be `false`.

## Queries

As this process is for htauth, and uses MySQL, an SQL query needs to be written to check the database for information.

This query should:
 - Only return the field `password`
 - Only return 0 or 1 row

Use `%s` where you want the `username` to be queried.

Example Query: `select password from users where email="%s" limit 1`

Make sure that quotes are properly escaped. Once in the `nginx.config` the above query would need `\\"%s\\"`

## Database

As Configured, The passwords in the database must be an SHA-512 hashed password and must contain the `{SHA512-CRYPT}` tag at the beginning. EX: `{SHA512-CRYPT}$6$ZqU....gPA$broUR....8xeg1`

Changing this requires modification of the `mysql-htpasswd.lua` within the **`CHANGABLE LINES TO MODIFY PASSWORD CONFIGURATION`** area. This is not something I intend to support, so you're on your own.

## Passwords

SHA512 Passwords can be generated using one of two ways (see below). Passwords generated with `doveadm` automatically contain the `{SHA512-CRYPT}` tag, but if using `openssl`, this tag needs to be added.

### Generating with OpenSSL

1. Ensure `openssl` package is installed and `openssl` is an available command.
2. Create a password with: `openssl passwd -6` 

### Generating with Dovecot admin

1. Ensure `dovecot-core` package is installed and `doveadm` is an available command.
2. Create a password with: `doveadm pw -s SHA512-CRYPT`
