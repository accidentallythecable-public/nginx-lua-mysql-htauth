local b64 = require "ngx.base64"
local mysql = require "resty.mysql"
local shell = require "resty.shell"

local function split(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        local i=0
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end

local db, err = mysql:new()
if not db then
    ngx.log(ngx.STDERR, "Unable to init db for authentication:", err)
    ngx.exit(500)
end

db:set_timeout(1000)

-- ngx.log(ngx.STDERR, ngx.req.raw_header())

local encoded = ngx.req.get_headers()["Authorization"]
if encoded ~= nil then
    -- ngx.log(ngx.STDERR, "Got Authorization Header")
    
    local ok, err, errcode, sqlstate = db:connect{
        host = ngx.var.ngx_mysql_host,
        port = ngx.var.ngx_mysql_port,
        database = ngx.var.ngx_mysql_database,
        user = ngx.var.ngx_mysql_user,
        password = ngx.var.ngx_mysql_password,
        charset = "utf8",
        max_packet_size = 1024^2,
        ssl = ngx.var.ngx_mysql_ssl,
        ssl_verify = ngx.var.ngx_mysql_ssl
    }
    if not ok then
        ngx.log(ngx.STDERR, "Unable to connect for authentication (", errcode, "): ", err)
        ngx.exit(500)
    end
    -- ngx.log(ngx.STDERR, "Connected...")
    
    local auth_data = split(encoded, ' ')
    if (table.getn(auth_data) + 1) ~= 2 then
        ngx.log(ngx.STDERR, "Invalid header format, should've gotten 2 parts from '",encoded,"', but got: ",(table.getn(auth_data) + 1))
        ngx.exit(400)
    end
    local auth_token_enc = auth_data[1]
    local decoded = b64.decode_base64url(auth_token_enc)
    local auth_token_dec = split(decoded, ':')

    local auth_user = auth_token_dec[0]
    local auth_pass = auth_token_dec[1]

    -- local res, err, errcode, sqlstate = db:query("select password from virtual_users where email=\""..auth_user.."\" limit 1",1)
    local res, err, errcode, sqlstate = db:query(string.format(ngx.var.ngx_mysql_select_stmt,auth_user),1)
    if err ~= nil then
        ngx.log(ngx.STDERR, "Unable to query for authentication (", errcode, "): ", err)
        ngx.exit(500)
    end
    if table.getn(res) == 0 then
        ngx.log(ngx.STDERR, "Query returned zero rows for ",auth_user)
        ngx.exit(403)      
    end
    local row = table.unpack(res)
    local tgt_pass = row.password

    local in_pieces = split(tgt_pass,'$')
    local rounds = in_pieces[1]
    local salt = in_pieces[2]
    local tgt_crypt = in_pieces[3]


    local shell = require "resty.shell"
    local ok, stdout, stderr, reason, status = shell.run({"openssl", "passwd", "-6", "-noverify", "-stdin", "-salt", salt},auth_pass,1000,4096)
    if not ok then
        ngx.log(ngx.STDERR, "Failed to encrypt incoming password")
        ngx.log(ngx.STDERR, "Error: "..stderr..", Reason: "..reason..", Status: "..status)
        ngx.exit(500)
    end
    local enc_pass = split(stdout,'\n')
    local check_pass = "{SHA512-CRYPT}"..(enc_pass[0])

    if tgt_pass ~= check_pass then
        -- ngx.log(ngx.STDERR, "Check  : '"..tgt_pass.."'")
        -- ngx.log(ngx.STDERR, "Compare: '"..check_pass.."'")
        ngx.log(ngx.STDERR, "Invalid User/Pass for '"..auth_user.."'")
        ngx.exit(403)
    end
    ngx.log(ngx.STDERR, "Success!")
    local ok, err = db:close()
    if not ok then
        ngx.log(ngx.STDERR, "Failed Closing Connection: ", err)
        ngx.exit(500)
    end
    ngx.log(ngx.STDERR, "User '"..auth_user.."' Successfully authenticated")
    return
else
    -- ngx.log(ngx.STDERR, "NO Header")
    ngx.header["WWW-Authenticate"] = "Basic realm=\""..ngx.var.auth_realm.."\", charset=\"UTF-8\""
    ngx.exit(401)
end
